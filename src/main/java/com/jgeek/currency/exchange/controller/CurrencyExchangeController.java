package com.jgeek.currency.exchange.controller;

import com.jgeek.currency.exchange.repository.CurrencyExchangeRepository;
import com.jgeek.currency.exchange.model.CurrencyExchange;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class CurrencyExchangeController {

    @Autowired
    CurrencyExchangeRepository currencyExchangeRepository;

    @Autowired
    Environment environment;

    @GetMapping("/currency-exchange/{from}/{to}")
    public CurrencyExchange retrieveExchangeValue
            (@PathVariable String from, @PathVariable String to) {

        CurrencyExchange exchangeValue =
                currencyExchangeRepository.findByFromAndTo(from, to);

        exchangeValue.setPort(
                Integer.parseInt(environment.getProperty("local.server.port")));

        log.info("{}", exchangeValue);

        return exchangeValue;
    }
}
