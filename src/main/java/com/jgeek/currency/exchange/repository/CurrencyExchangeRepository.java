package com.jgeek.currency.exchange.repository;

import com.jgeek.currency.exchange.model.CurrencyExchange;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyExchangeRepository extends JpaRepository<CurrencyExchange, Long>{
    CurrencyExchange  findByFromAndTo(String from, String to);
}
